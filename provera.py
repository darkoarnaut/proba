# -*- coding: utf-8 -*-
"""
Created on Sat Mar 28 10:57:33 2020

@author: Arnaut
"""
import tensorflow.keras as keras
import numpy as np
import random
import librosa
import os
def konvertuj(file):
    res=file[0]
    resenje=[0,0,0,0,0,0,0,0,0,0]
    resenje[int(res)]=1
   # print(resenje)
    return resenje

def ucitaj(imeDir):
    files = os.listdir(imeDir)
    random.shuffle(files)
    mfccs=[]
    resenja=[]
    for file in files:
        (sig, rate) = librosa.load(imeDir+'/'+file, sr=None)
        mfcc=librosa.feature.mfcc(y=sig, sr=rate)
        #print(mfcc)
        if mfcc.shape[1] < 20:
            mfcc = np.pad(mfcc, ((0, 0), (0, 20 - mfcc.shape[1])),mode='constant', constant_values=0)
            mfcc=mfcc.T
            #print(mfcc.shape)
            resenje=konvertuj(file)
            mfccs.append(mfcc)
            resenja.append(resenje)
    return mfccs,resenja

json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
model = keras.models.model_from_json(loaded_model_json)
# load weights into new model
model.load_weights("tezine.h5")
print("Loaded model from disk")

x_test,y_test=ucitaj('test')
y_test=np.array(y_test)
x_test=np.array(x_test)
y_test=np.argmax(y_test,axis=1)

print("resenje")
print(y_test)
prediction=model.predict_classes(x_test)
print("odgovor mreze")
print(prediction)

