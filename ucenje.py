# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 14:35:28 2020

@author: Arnaut
"""

import numpy as np
import librosa
import os
import random
import tensorflow.keras as keras
def konvertuj(file):
    res=file[0]
    resenje=[0,0,0,0,0,0,0,0,0,0]
    resenje[int(res)]=1
   # print(resenje)
    return resenje

def ucitaj(imeDir):
    files = os.listdir(imeDir)
    random.shuffle(files)
    mfccs=[]
    resenja=[]
    for file in files:
        (sig, rate) = librosa.load(imeDir+'/'+file, sr=None)
        mfcc=librosa.feature.mfcc(y=sig, sr=rate)
        #print(mfcc)
        if mfcc.shape[1] < 20:
            mfcc = np.pad(mfcc, ((0, 0), (0, 20 - mfcc.shape[1])),mode='constant', constant_values=0)
            mfcc=mfcc.T
            #print(mfcc.shape)
            resenje=konvertuj(file)
            mfccs.append(mfcc)
            resenja.append(resenje)
    return mfccs,resenja

"""(sig, rate) = librosa.load('9_yweweler_40.wav', sr=None)
mfcc=librosa.feature.mfcc(y=sig, sr=rate, n_mfcc=13).T
print(mfcc.shape)
"""
x_train,y_train=ucitaj('recordings')
x_test,y_test=ucitaj('dev')
x_train_np=np.array(x_train)
y_train_np=np.array(y_train)
x_test_np=np.array(x_test)
y_test_np=np.array(y_test)
y_train_np = np.argmax(y_train_np, axis=1)
y_test_np = np.argmax(y_test_np, axis=1)


model=keras.Sequential([
    keras.layers.Flatten(input_shape=(x_train_np.shape[1], x_train_np.shape[2])),
    keras.layers.Dense(1024,activation="softsign"),
    keras.layers.Dense(10, activation="softmax")
    ])


optimizer=keras.optimizers.Adam(learning_rate=0.0001)
model.compile(optimizer=optimizer,loss="sparse_categorical_crossentropy",
              metrics=["accuracy"])
model.summary()

model.fit(x_train_np,y_train_np, validation_data=(x_test_np,y_test_np),
          epochs=50, batch_size=32)


modelJson=model.to_json()
with open("model.json","w") as jsonfile:
    jsonfile.write(modelJson)
model.save_weights("tezine.h5")
"""samplingFrequency, signalData = wavfile.read('9_yweweler_40.wav') 
 
# Plot the signal read from wav file
plot.subplot(211)
plot.title('Spectrogram of a wav file with piano music')
 
plot.plot(signalData)
plot.xlabel('Sample')
plot.ylabel('Amplitude')
 
plot.subplot(212)
plot.specgram(signalData,Fs=samplingFrequency) 
plot.xlabel('Time')
plot.ylabel('Frequency')
 
plot.show()"""
